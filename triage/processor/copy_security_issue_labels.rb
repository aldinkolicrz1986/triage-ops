# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/related_issue_finder'
require_relative '../triage/unique_comment'

module Triage
  class CopySecurityIssueLabels < Processor
    react_to 'issue.open', 'issue.update'

    SECURITY_GITLAB_PROJECT_PATH = "#{GITLAB_ORG_SECURITY_GROUP}/gitlab".freeze
    DESCRIPTION_MAIN_ISSUE_LINK_REGEXP = %r{Issue on \[GitLab\].*\|\s*https://gitlab\.com/(?<project_id>.*)/-/issues/(?<iid>\d+)\s*\|$}

    REQUIRED_LABELS = [
      'security', 'vulnerability', 'breaking change',
      'severity::*', 'type::*', 'group::*',
      'devops::*', 'section::*', 'backend', 'frontend'
    ].freeze

    def self.required_labels
      @required_labels ||= REQUIRED_LABELS.map do |label_pattern|
        label_regexp = label_pattern.gsub('*', '.*')
        /^#{label_regexp}$/
      end
    end

    def applicable?
      event.from_gitlab_org_security? &&
        security_implementation_issue? &&
        main_issue &&
        unique_comment.no_previous_comment? &&
        labels_to_add.any?
    end

    def process
      add_comment(comment_body, append_source_link: true)
    end

    def documentation
      <<~TEXT
        This processor copies required labels from main issue to security implementation issue for better discovery and triaging.
      TEXT
    end

    private

    def security_implementation_issue?
      event.project_path_with_namespace == SECURITY_GITLAB_PROJECT_PATH
    end

    def main_issue
      @main_issue ||= Triage::RelatedIssueFinder.new(related_issue_regex: DESCRIPTION_MAIN_ISSUE_LINK_REGEXP).find_issue_in(event.description)
    end

    def labels_to_add
      @labels_to_add ||= begin
        missing_label_regexes = self.class.required_labels.reject do |required_label_regex|
          event.label_names.any? { |name| required_label_regex.match?(name) }
        end

        if missing_label_regexes.empty? || !main_issue
          []
        else
          missing_label_regexes.flat_map do |missing_label_regex|
            main_issue.labels.grep(missing_label_regex)
          end
        end
      end
    end

    def markdown_labels_to_add
      @markdown_labels_to_add ||= labels_to_add.map { |l| %(~"#{l}") }.join(' ')
    end

    def comment_body
      comment = <<~MARKDOWN.chomp
        Copied #{markdown_labels_to_add} from main project issue.
        /label #{markdown_labels_to_add}
      MARKDOWN

      unique_comment.wrap(comment).strip
    end
  end
end
