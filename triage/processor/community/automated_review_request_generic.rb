# frozen_string_literal: true

require_relative 'community_processor'
require_relative '../../../lib/team_member_select_helper'

module Triage
  class AutomatedReviewRequestGeneric < CommunityProcessor
    include TeamMemberSelectHelper

    GROUP_LABEL_PREFIX = 'group::'
    GROUP_LABEL_REGEX = /^#{GROUP_LABEL_PREFIX}(?<group_name>.*)$/
    PROJECTS_WITH_EXTERNAL_REVIEW_PROCESSES = [
      # https://gitlab.com/gitlab-org/security-products/gemnasium-db
      # The Vulnerability Research team has a rotating
      # reviewer for this project who reviews all MRs.
      12006272,
      # Exclude select Quality projects that have their own workflow
      # https://gitlab.com/gitlab-org/gitlab-environment-toolkit
      # https://gitlab.com/gitlab-org/quality/performance
      14292404,
      11511606
    ].freeze

    react_to 'merge_request.update'

    def initialize(event)
      super(event)

      @discord_messenger = DiscordMessenger.new(ENV.fetch('DISCORD_WEBHOOK_PATH_COMMUNITY_MR_REQUEST_REVIEW', nil))
    end

    def applicable?
      wider_community_contribution_open_resource? &&
        workflow_ready_for_review_added? &&
        !project_with_external_review_process?
    end

    def process
      post_review_request_comment

      post_discord_message if event.project_public?
    end

    private

    attr_reader :discord_messenger

    def project_with_external_review_process?
      PROJECTS_WITH_EXTERNAL_REVIEW_PROCESSES.include?(event.project_id) ||
        distribution_project?
    end

    # The Distribution team has their own review workflow so we don't assign until we're sure they're ok with this new workflow
    # See https://about.gitlab.com/handbook/engineering/development/enablement/distribution/merge_requests.html#workflow
    def distribution_project?
      WwwGitLabCom.distribution_projects.include?(event.project_id)
    end

    def post_review_request_comment
      comment = <<~MARKDOWN.strip
      #{intro_sentence}

      - Do you have capacity and domain expertise to review this? If not, find one or more [reviewers](https://gitlab-org.gitlab.io/gitlab-roulette/) and assign to them.
      - If you've reviewed it, add the ~"#{Labels::WORKFLOW_IN_DEV_LABEL}" label if these changes need more work before the next review.

      /unlabel ~"#{Labels::AUTOMATION_AUTHOR_REMINDED_LABEL}"
      MARKDOWN
      add_comment(comment, append_source_link: true)
    end

    def intro_sentence
      if current_reviewers.any?
        <<~MARKDOWN.strip
        #{current_reviewers.join(' ')}, this ~"#{Labels::COMMUNITY_CONTRIBUTION_LABEL}" is ready for review.
        MARKDOWN
      else
        <<~MARKDOWN.strip
        Hi Coach `#{coach}`, this ~"#{Labels::COMMUNITY_CONTRIBUTION_LABEL}" is ready for review or needs your coaching.

        /assign_reviewer #{coach}
        MARKDOWN
      end
    end

    def current_reviewer_usernames
      @current_reviewer_usernames ||= merge_request.reviewers.map(&:username)
    end

    def current_reviewers
      @current_reviewers ||= current_reviewer_usernames.map { |reviewer| "`@#{reviewer}`" }
    end

    def merge_request
      @merge_request ||= Triage.api_client.merge_request(event.project_id, event.iid)
    end

    def coach
      @coach ||= select_random_merge_request_coach(group: group)
    end

    def group
      event.label_names.grep(GROUP_LABEL_REGEX).first&.delete_prefix(GROUP_LABEL_PREFIX)
    end

    def format_current_reviewers
      return "`#{coach.delete_prefix('@')}`" unless current_reviewers.any?

      "`#{current_reviewer_usernames.join('`, `')}`"
    end

    def post_discord_message
      message = <<~TEXT
        A merge request from `#{event.resource_author.username}` is ready to be reviewed:
        - URL: #{event.url}
        - Title: `#{event.title}`
        - Project: `#{event.project_path_with_namespace}`
        - Labels: #{format_current_labels_list}
        - Reviewers: #{format_current_reviewers}
      TEXT

      discord_messenger.send_message(message)
    end
  end
end
