# frozen_string_literal: true

require_relative '../triage'
require_relative '../triage/job'
require_relative '../triage/reaction'
require_relative '../triage/unique_comment'

module Triage
  class BrokenMasterLabelNudgerJob < Job
    include Reaction

    ROOT_CAUSE_LABEL_MISSING_MESSAGE = 'please add a concrete `~master-broken::` root cause label'
    FLAKY_REASON_LABEL_MISSING_MESSAGE = 'please identify the flaky root cause with a `~flaky-test::` label'

    private

    def execute(event)
      prepare_executing_with(event)

      add_comment(comment, append_source_link: true) if still_missing_root_cause_label? || still_missing_flaky_reason_label?
    end

    def still_missing_root_cause_label?
      issue.labels.include?(Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS[:default])
    end

    def still_missing_flaky_reason_label?
      issue.labels.include?(Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS[:flaky_test]) &&
        issue.labels.none? { |label| label.start_with? Labels::FLAKY_TEST_LABEL_PREFIX }
    end

    def issue
      @issue ||= Triage.api_client.issue(event.project_id, event.iid)
    end

    def comment
      message = still_missing_root_cause_label? ? ROOT_CAUSE_LABEL_MISSING_MESSAGE : FLAKY_REASON_LABEL_MISSING_MESSAGE

      message_markdown = <<~MARKDOWN.chomp
        :wave: @#{event.event_actor_username}, #{message} following the [Triage DRI Responsibilities handbook page](https://about.gitlab.com/handbook/engineering/workflow/#triage-dri-responsibilities).

        Ignore this comment if you think the incident already has all of the necessary labels to conclude your root cause analysis.
      MARKDOWN

      unique_comment.wrap(message_markdown).strip
    end

    def unique_comment
      @unique_comment ||= UniqueComment.new("Triage::BrokenMasterLabelNudgerJob", event)
    end
  end
end
