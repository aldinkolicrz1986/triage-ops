# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/backstage_label'
require_relative '../../triage/triage/event'

RSpec.describe Triage::BackstageLabel do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      {
        object_kind: 'issue',
        action: 'open',
        from_gitlab_org?: true,
        added_label_names: added_label_names
      }
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ["issue.close", "issue.note", "issue.open", "issue.reopen", "issue.update", "merge_request.approval", "merge_request.approved", "merge_request.close", "merge_request.merge", "merge_request.note", "merge_request.update", "merge_request.open", "merge_request.reopen", "merge_request.unapproval", "merge_request.unapproved"]

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when there is no backstage label' do
      let(:added_label_names) { ['foo'] }

      include_examples 'event is not applicable'
    end

    described_class::BACKSTAGE_LABELS.each do |label|
      context 'when there is the backstage label' do
        let(:added_label_names) { [label] }

        include_examples 'event is applicable'
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:label) { described_class::BACKSTAGE_LABELS.first }
    let(:added_label_names) { [label] }

    it 'posts a deprecation message' do
      body = add_automation_suffix('processor/backstage_label.rb') do
        <<~MARKDOWN.chomp
          Hey @root, ~"#{label}" is being deprecated in favor of ~"type::feature", ~"feature::addition", ~"type::maintenance", ~"maintenance::pipelines", and ~"maintenance::workflow" to improve the identification of these type of changes.
          Please see https://about.gitlab.com/handbook/engineering/metrics/#data-classification for further guidance.
          /unlabel ~"#{label}"
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
