# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/code_review_experience_feedback'

RSpec.describe Triage::CodeReviewExperienceFeedback do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        object_kind: 'merge_request'
      }
    end
  end

  include_context 'with merge request notes'

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.merge', 'merge_request.close']

  describe '#applicable?' do
    it_behaves_like 'community contribution processor #applicable?'

    context 'when there is already a comment for the same purpose' do
      let(:merge_request_notes) do
        [
          { body: 'review comment 1' },
          { body: comment_mark }
        ]
      end

      include_examples 'event is not applicable'
    end

    context 'when the merge request is labelled as spam' do
      let(:label_names) { ['Spam'] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts code review experience message' do
      body = add_automation_suffix('processor/community/code_review_experience_feedback.rb') do
        <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          @#{event.resource_author.username}, how was your code review experience with this merge request? Please tell us how we can continue to [iterate](https://about.gitlab.com/handbook/values/#iteration) and improve:

          1. React with a :thumbsup: or a :thumbsdown: on this comment to describe your experience.
          1. Create a new comment starting with `@gitlab-bot feedback` below, and leave any additional feedback you have for us in the comment.

          Interested in learning more tips and tricks to solve your next challenge faster? [Subscribe to the GitLab Community Newsletter](https://about.gitlab.com/community/newsletter/) for contributor-focused content and opportunities to level up.

          Thanks for your help! :heart:
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
