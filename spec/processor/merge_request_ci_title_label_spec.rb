# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/merge_request_ci_title_label'
require_relative '../../triage/triage/event'

RSpec.describe Triage::MergeRequestCiTitleLabel do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'open',
        from_gitlab_org_gitlab?: from_gitlab_org_gitlab,
        title: title,
        label_names: labels
      }
    end

    let(:original_title) { 'My GitLab MR' }
    let(:title) { "#{original_title} [RUN ALL RSPEC]" }
    let(:labels) { [] }
    let(:from_gitlab_org_gitlab) { true }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ["merge_request.approval", "merge_request.approved", "merge_request.close", "merge_request.merge", "merge_request.note", "merge_request.open", "merge_request.reopen", "merge_request.unapproval", "merge_request.unapproved", "merge_request.update"]

  describe '#applicable?' do
    context 'when there is no matching title' do
      let(:title) { original_title }

      include_examples 'event is not applicable'
    end

    context 'when project applicable' do
      let(:from_gitlab_org_gitlab) { false }

      context 'when project is not gitlab-org/gitlab' do
        before do
          allow(event).to receive(:with_project_id?).and_return(false)
        end

        include_examples 'event is not applicable'
      end

      context 'when alternative project' do
        let(:alt_project_id) { 999 }

        before do
          stub_env('ALT_PROJECT_ID_FOR_CI_TITLE_LABEL' => alt_project_id)
          allow(event).to receive(:with_project_id?).with(alt_project_id).and_return(true)
        end

        include_examples 'event is applicable'
      end
    end

    context 'when resource is not opened' do
      before do
        allow(event).to receive(:resource_open?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when there is a matching title' do
      ["RUN ALL RSPEC", "RUN AS-IF-FOSS", "UPDATE CACHE", "SKIP RSPEC FAIL-FAST"].each do |ci_title|
        let(:title) { [original_title, ci_title].join(' ') }

        include_examples 'event is applicable'
      end
    end
  end

  context 'when title to label expectations' do
    where(:case_description, :ci_title, :expected_labels) do
      [
        ["Run All Rspec", "#{original_title} RUN ALL RSPEC", [described_class::RUN_ALL_RSPEC_LABEL]],
        ["Run As if foss", "(RUN AS-IF-FOSS) #{original_title}", [described_class::RUN_AS_IF_FOSS_LABEL]],
        ["Update cache", "#{original_title} UPDATE CACHE", [described_class::UPDATE_CACHE_LABEL]],
        ["SKIP RSPEC FAIL-FAST", "#{original_title} [SKIP RSPEC FAIL-FAST]", [described_class::SKIP_RSPEC_FAIL_FAST_LABEL]],
        ["Two terms", "[RUN ALL RSPEC] #{original_title} [UPDATE CACHE]", [described_class::RUN_ALL_RSPEC_LABEL, described_class::UPDATE_CACHE_LABEL]]
      ]
    end

    with_them do
      let(:title) { ci_title }

      context 'without labels' do
        describe '#applicable' do
          include_examples 'event is applicable'
        end

        describe '#labels_for_ci_title' do
          it 'results the correct labels' do
            expect(subject.labels_for_ci_title.map(&:name)).to eq(expected_labels)
          end
        end

        describe '#documentation' do
          it_behaves_like 'processor documentation is present'
        end

        describe '#process' do
          it 'results the correct message' do
            labels = expected_labels.map { |label| "~\"#{label}\"" }.join(' ')

            body = <<~MARKDOWN.chomp
              /label #{labels}
            MARKDOWN

            expect_comment_request(event: event, body: body) do
              subject.process
            end
          end
        end

        describe '#merge_request_title_match_ci_flag?' do
          it 'returns true' do
            expect(subject.merge_request_title_match_ci_flag?).to be(true)
          end
        end

        describe '#ci_labels_missing?' do
          it 'returns true' do
            expect(subject.ci_labels_missing?).to be(true)
          end
        end
      end

      context 'with labels already' do
        let(:labels) { expected_labels }

        describe '#applicable' do
          include_examples 'event is not applicable'
        end

        describe '#ci_labels_missing?' do
          it 'returns false' do
            expect(subject.ci_labels_missing?).to be(false)
          end
        end

        describe '#merge_request_title_match_ci_flag?' do
          it 'returns true even with labels already' do
            expect(subject.merge_request_title_match_ci_flag?).to be(true)
          end
        end
      end
    end
  end
end
