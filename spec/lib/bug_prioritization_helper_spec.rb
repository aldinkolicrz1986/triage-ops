# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/bug_prioritization_helper'

RSpec.describe BugPrioritizationHelperContext do
  let(:resource_klass) do
    Class.new do
      include BugPrioritizationHelperContext
    end
  end

  let(:resource) { resource_klass.new }

  before do
    stub_request(:get, "https://gitlab.com/api/v4/groups/9970/milestones?state=active ")
      .to_return(status: 200, body: read_fixture('milestones.json'), headers: {})
  end

  around do |example|
    Timecop.freeze(2023, 11, 1) { example.run }
  end

  describe '#bug_prioritization_summary' do
    subject(:group_summary) { resource.bug_prioritization_summary(items: items, assignees: assignees, labels: labels, mentions: mentions) }

    let(:items) do
      (1..16).map { |i| "issue#{i}" }.join("\n")
    end

    it_behaves_like 'report summaries' do
      describe 'milestone' do
        it 'shows upcoming milestone title in description' do
          expect(group_summary).to include('upcoming milestone %"16.7"')
        end
      end

      describe 'due date' do
        it 'sets a due date' do
          expect(subject).to match("/due #{described_class::BUG_REPORT_DUE}")
        end
      end

      describe 'issues' do
        it 'limits the issue count to 15' do
          expect(subject).not_to include("issue16")
        end
      end
    end
  end

  describe '#bug_prioritization_title' do
    subject(:summary_title) { resource.bug_prioritization_title(group_name) }

    context 'when a group name is provided and milestone is fetched from api' do
      let(:group_name) { 'group::somegroup' }

      it 'shows the group name in the title' do
        expect(summary_title).to match(group_name)
      end

      it 'shows upcoming milestone title' do
        expect(summary_title).to include('16.7')
      end
    end
  end

  describe '#upcoming_milestone_title' do
    it 'returns the upcoming milestone' do
      expect(resource.send(:upcoming_milestone_title)).to match("16.7")
    end
  end
end
