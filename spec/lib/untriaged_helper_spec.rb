# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/untriaged_helper'

RSpec.describe UntriagedHelper do
  let(:resource_klass) do
    Struct.new(:labels) do
      include UntriagedHelper
    end
  end

  let(:label_klass) do
    Struct.new(:name)
  end

  let(:labels) { [] }

  let(:team_from_www) do
    {
      'user1' => { 'departments' => ['Quality Department', 'Merge Request coach'] },
      'user2' => { 'departments' => ['Quality Department', 'Merge Request coach'] },
      'user3' => { 'departments' => ['Quality Department'] },
      'user4' => { 'role' => 'Director of Quality', 'departments' => ['Quality Department'] },
      'user5' => { 'role' => 'Senior Operations Analyst', 'departments' => ['Quality Department'] },
      'user6' => { 'role' => 'Staff Infrastructure Analyst', 'departments' => ['Quality Department'] },
      'user7' => { 'role' => 'Vice President of Quality', 'departments' => ['Quality Department'] },
      'user8' => { 'role' => 'Engineering Manager, Engineering Productivity', 'departments' => ['Quality Department'] },
      'user9' => { 'role' => 'Quality Engineering Manager, Engineering Productivity', 'departments' => ['Quality Department'] }
    }
  end

  let(:roulette) do
    [
      { 'username' => 'user1' },
      { 'username' => 'user2', 'out_of_office' => true },
      { 'username' => 'user3', 'out_of_office' => false },
      { 'username' => 'user4', 'out_of_office' => false }
    ]
  end

  subject { resource_klass.new(labels) }

  describe '#distribute_items' do
    let(:list_items) { (1..7).to_a.map { |i| "Item ##{i}" } }
    let(:potential_triagers) { %w[@triager-3 @triager-2 @triager-1] }

    before do
      allow(subject).to receive(:puts)
    end

    it 'distributes items all items across triagers' do
      distribution = subject.distribute_items(list_items, potential_triagers)
      items = distribution.values.sum([])

      expect(items).to match_array(list_items)
    end

    it 'sorts triagers by username' do
      distribution = subject.distribute_items(list_items, potential_triagers)
      triagers = distribution.keys

      expect(triagers).to eq(potential_triagers.sort)
    end

    context 'when there is no triagers available' do
      it 'returns an empty hash' do
        distribution = subject.distribute_items(list_items, [])

        expect(distribution).to eq({})
      end
    end
  end

  describe '#distribute_and_display_items_per_triager' do
    let(:list_items) { (1..7).to_a.map { |i| "Item ##{i}" } }

    before do
      allow(subject).to receive(:puts)
    end

    context 'with empty triagers' do
      let(:triagers) { [] }
      let(:items_text) do
        <<~MARKDOWN.chomp
          `unassigned`

          Item #1
          Item #2
          Item #3
          Item #4
          Item #5
          Item #6
          Item #7
        MARKDOWN
      end

      it 'distributes items but do not assign anyone' do
        displayed_text = subject.distribute_and_display_items_per_triager(list_items, triagers)

        expect(displayed_text).to eq(items_text)
      end
    end

    context 'with valid triagers' do
      let(:triagers) { ['@em1', '@qem2'] }

      it 'distributes items to triagers' do
        displayed_text = subject.distribute_and_display_items_per_triager(list_items, triagers)

        expect(displayed_text).to include("\n/assign @em1 @qem2")
      end
    end
  end

  describe '#untriaged?' do
    context 'when resource is triaged' do
      context 'with special issue label' do
        described_class::SPECIAL_ISSUE_LABELS.each do |special_issue_label|
          context "when special issue label is #{special_issue_label}" do
            let(:labels) { [label_klass.new(special_issue_label)] }

            it 'returns false' do
              expect(subject.untriaged?).to be(false)
            end
          end
        end
      end

      context 'with type::ignore label' do
        let(:labels) { [label_klass.new('type::ignore')] }

        it 'returns false' do
          expect(subject.untriaged?).to be(false)
        end
      end

      context 'with type, and department label' do
        described_class::TYPE_LABELS.each do |type_label|
          context "when type label is #{type_label}" do
            let(:labels) { [label_klass.new(type_label), label_klass.new('Engineering Productivity')] }

            it 'returns false' do
              expect(subject.untriaged?).to be(false)
            end
          end
        end
      end

      context 'with type, stage and group labels' do
        described_class::TYPE_LABELS.each do |type_label|
          context "when type label is #{type_label}" do
            let(:labels) { [label_klass.new('group::runner'), label_klass.new('devops::verify'), label_klass.new(type_label)] }

            it 'returns false' do
              expect(subject.untriaged?).to be(false)
            end
          end
        end
      end
    end

    context 'when resource is untriaged' do
      context 'with no stage label' do
        described_class::TYPE_LABELS.each do |type_label|
          context "when type label is #{type_label}" do
            let(:labels) { [label_klass.new('group::runner'), label_klass.new(type_label)] }

            it 'returns true' do
              expect(subject.untriaged?).to be(true)
            end
          end
        end
      end

      context 'with no group label' do
        described_class::TYPE_LABELS.each do |type_label|
          context "when type label is #{type_label}" do
            let(:labels) { [label_klass.new('devops::verify'), label_klass.new(type_label)] }

            it 'returns true' do
              expect(subject.untriaged?).to be(true)
            end
          end
        end
      end

      context 'with no type label' do
        let(:labels) { [label_klass.new('group::runner'), label_klass.new('devops::verify')] }

        it 'returns true' do
          expect(subject.untriaged?).to be(true)
        end
      end
    end

    context 'with only a department label' do
      let(:labels) { [label_klass.new('Engineering Productivity')] }

      it 'returns true' do
        expect(subject.untriaged?).to be(true)
      end
    end

    context 'with no label' do
      it 'returns true' do
        expect(subject.untriaged?).to be(true)
      end
    end
  end

  describe '#merge_requests_from_line_items' do
    let(:items) do
      [
        '@user1,https://gitlab/merge-request/1,label1',
        '@user2,https://gitlab/merge-request/2,label2,label3',
        '@user1,https://gitlab/merge-request/3,label1'
      ].join("\n")
    end

    it 'returns list of merge requests' do
      expected = [
        { author: '@user1', url: 'https://gitlab/merge-request/1', labels: ['label1'] },
        { author: '@user2', url: 'https://gitlab/merge-request/2', labels: %w[label2 label3] },
        { author: '@user1', url: 'https://gitlab/merge-request/3', labels: ['label1'] }
      ]

      expect(subject.merge_requests_from_line_items(items)).to eq(expected)
    end
  end

  describe '#special_issue?' do
    context 'when labels do not include a special issue label' do
      let(:labels) { [label_klass.new('type::bug')] }

      it 'returns false' do
        expect(subject).not_to be_special_issue
      end
    end

    context 'when labels include a special issue label' do
      let(:labels) { [label_klass.new('triage report')] }

      it 'returns true' do
        expect(subject).to be_special_issue
      end
    end
  end

  describe '#has_subtype_label' do
    context 'when labels do not include a subtype label' do
      let(:labels) { [label_klass.new('bug::not_exist')] }

      it 'returns false' do
        expect(subject).not_to have_subtype_label
      end
    end

    context 'when labels include a subtype label' do
      described_class::SUBTYPE_LABELS.each do |subtype_label|
        let(:labels) { [label_klass.new(subtype_label)] }

        it "returns true when subtype label is #{subtype_label}" do
          expect(subject).to have_subtype_label
        end
      end
    end
  end

  describe '#issue_labels_missing' do
    context 'when issue has type, subtype, and special issue labels' do
      let(:labels) do
        [label_klass.new('type::maintenance'),
          label_klass.new('maintenance::refactor'),
          label_klass.new('meta')]
      end

      it 'returns false' do
        expect(subject.issue_labels_missing?).to be(false)
      end
    end

    context 'when issue has type::ignore label' do
      let(:labels) { [label_klass.new('type::ignore')] }

      it 'returns false' do
        expect(subject.issue_labels_missing?).to be(false)
      end
    end

    context 'when issue has no type, subtype, or special issue labels' do
      let(:labels) { [label_klass.new('Engineering Productivity')] }

      it 'returns true' do
        expect(subject.issue_labels_missing?).to be(true)
      end
    end
  end

  describe '#has_type_ignore_label?' do
    context 'without type::ignore label' do
      it 'returns false' do
        expect(subject.has_type_ignore_label?).to be(false)
      end
    end

    context 'with type::ignore label' do
      let(:labels) { [label_klass.new('type::ignore')] }

      it 'returns true' do
        expect(subject.has_type_ignore_label?).to be(true)
      end
    end
  end
end
