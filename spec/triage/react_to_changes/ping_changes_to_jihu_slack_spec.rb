# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/react_to_changes'
require_relative '../../../triage/triage/react_to_changes/ping_changes_to_jihu_slack'

RSpec.describe Triage::PingChangesToJiHuSlack do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        merge_event?: merge_event
      }
    end

    let(:eslint_change) do
      {
        'old_path' => '.eslintrc.yml',
        'new_path' => '.eslintrc.yml',
        'diff' => '+ "require-await": error'
      }
    end

    let(:merge_request_changes) do
      { 'changes' => [eslint_change].compact }
    end

    let(:slack_webhook_url) { 'http://slack-webhook-url' }
    let(:merge_event) { true }
  end

  let(:processor) { Triage::ReactToChanges.new(event) }

  subject { described_class.new(processor) }

  before do
    stub_env('JH_SLACK_WEBHOOK_URL' => slack_webhook_url)

    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{iid}/changes",
      response_body: merge_request_changes)
  end

  describe '#applicable?' do
    include_examples 'applicable on contextual event'

    context 'when it has no JH_SLACK_WEBHOOK_URL set' do
      let(:slack_webhook_url) { nil }

      it 'is not applicable' do
        expect(subject).not_to be_applicable
      end
    end

    context 'when it is not a merge event' do
      let(:merge_event) { false }

      it 'is not applicable' do
        expect(subject).not_to be_applicable
      end
    end

    context 'when there is no .eslintrc.yml changes' do
      let(:eslint_change) { nil }

      it 'is not applicable' do
        expect(subject).not_to be_applicable
      end
    end
  end

  describe '#react' do
    it 'posts a message to Slack based on changes' do
      payload = {
        'payload' => JSON.dump(
          'channel' => described_class::SLACK_CHANNEL,
          'username' => described_class::SLACK_USERNAME,
          'icon_emoji' => described_class::SLACK_ICON,
          'text' => "Changes for `.eslintrc.yml` detected in #{event.url}"
        )
      }

      request = stub_request(:post, slack_webhook_url).with(body: payload)

      subject.react

      expect(request).to have_been_requested
    end
  end
end
