# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../triage/triage'
require_relative '../../../triage/resources/commit'

RSpec.describe Triage::Commit do
  let(:commit_header) { 'this is a commit header' }
  let(:commit_body) { 'and this is a commit body' }
  let(:commit_attr) do
    {
      'message' => [commit_header, commit_body].compact.join("\n\n")
    }
  end

  subject { described_class.new(commit_attr) }

  describe '#header' do
    it 'returns the commit header' do
      expect(subject.header).to eq(commit_header)
    end

    context 'when commit body is not present' do
      let(:commit_body) { nil }

      it 'returns the commit header' do
        expect(subject.header).to eq(commit_header)
      end
    end
  end
end
