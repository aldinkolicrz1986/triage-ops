# frozen_string_literal: true

require 'spec_helper'
require 'slack-messenger'

require_relative '../../../triage/triage/event'
require_relative '../../../triage/triage/pipeline_failure/slack_notifier'
require_relative '../../../triage/triage/pipeline_failure/config/master_branch'
require_relative '../../../triage/triage/pipeline_failure/config/review_app_child_pipeline'

RSpec.describe Triage::PipelineFailure::SlackNotifier do
  include_context 'with data files stubbed'

  let(:project_path_with_namespace) { 'gitlab-org/gitlab' }
  let(:ref) { 'master' }
  let(:variables) do
    [
      { 'key' => 'SCHEDULE_TYPE', 'value' => 'nightly' }
    ]
  end

  let(:event) do
    instance_double(Triage::PipelineEvent,
      id: 42,
      project_path_with_namespace: project_path_with_namespace,
      project_id: 999,
      ref: ref,
      sha: 'sha',
      source_job_id: nil,
      web_url: 'web_url',
      commit_header: 'commit_header',
      event_actor: Triage::User.new(name: 'John Doe', username: 'john'),
      source: 'schedule',
      variables: variables,
      created_at: Time.now,
      merge_request: Triage::MergeRequest.new(title: 'Hello'),
      project_web_url: "https://gitlab.test/#{project_path_with_namespace}")
  end

  let(:config) { Triage::PipelineFailure::Config::MasterBranch.new(event) }
  let(:failed_jobs) do
    [
      double(name: 'foo', web_url: 'foo_web_url', allow_failure: false),
      double(name: 'bar', web_url: 'foo_web_url', allow_failure: false),
      double(name: 'baz', web_url: 'baz_web_url', allow_failure: false)
    ]
  end

  let(:incident_double) { double('Incident', project_id: '111', iid: 4567, web_url: 'incident_web_url', failed_jobs: failed_jobs, labels: %w[foo bar], state: 'closed') }
  let(:slack_client_double) { double('Slack::Messenger') }
  let(:pipeline_failure_config) { Triage::PipelineFailure::Config::MasterBranch.new(event) }

  subject { described_class.new(event: event, config: config, failed_jobs: failed_jobs, slack_webhook_url: 'slack_webhook_url', incident: incident_double) }

  around do |example|
    Timecop.freeze(Time.utc(2020, 3, 31, 8)) { example.run }
  end

  describe '#execute' do
    before do
      allow(Slack::Messenger).to receive(:new).and_return(slack_client_double)
    end

    shared_examples 'Slack message posting' do
      it 'posts a Slack message' do
        json_template = ERB.new(config.slack_payload_template, trim_mode: '-')
          .result_with_hash(incident: incident_double).squeeze("\n")

        expect(slack_client_double).to receive(:ping)
          .with(
            username: described_class::MESSAGE_AUTHOR,
            icon_emoji: described_class::MESSAGE_ICON_EMOJI,
            **JSON.parse(format(json_template, subject.__send__(:template_variables)))
          )

        subject.execute
      end
    end

    it_behaves_like 'Slack message posting'

    context 'without an incident' do
      let(:incident_double) { nil }

      it_behaves_like 'Slack message posting'
    end

    context 'when config contains empty default Slack channels' do
      before do
        allow(config).to receive(:default_slack_channels).and_return([])
      end

      it 'raises ArgumentError' do
        expect { subject.execute }.to raise_error(ArgumentError, "No Slack channels set!")
      end
    end

    context 'when variables do not include SCHEDULE_TYPE' do
      let(:variables) do
        [
          { 'key' => 'key', 'value' => 'value' }
        ]
      end

      it_behaves_like 'Slack message posting'
    end

    context 'with group labels' do
      let(:incident_labels) { ['group::group1'] }

      shared_examples 'notify incident to channels' do |channels|
        before do
          allow(slack_client_double).to receive(:ping)
          allow(incident_double).to receive(:labels).and_return(incident_labels)
        end

        it 'posts a Slack message to channels' do
          expect(Slack::Messenger).to receive(:new).with(
            'slack_webhook_url',
            { channel: channels, icon_emoji: ':boom:', username: '@gitlab-bot' }
          )

          subject.execute
        end
      end

      context 'with master broken incident containing group::environments label' do
        it_behaves_like 'notify incident to channels', %w[master-broken group1]
      end

      context 'with review-app incident containing a group label but not auto-triaged' do
        let(:config) { Triage::PipelineFailure::Config::ReviewAppChildPipeline.new(event) }

        it_behaves_like 'notify incident to channels', %w[review-apps-broken]
      end

      context 'with master broken incident containing an invalid group' do
        let(:incident_labels) { ['group::invalid'] }

        it_behaves_like 'notify incident to channels', %w[master-broken]
      end
    end
  end
end
