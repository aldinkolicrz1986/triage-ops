add-milestone-to-community-merge-requests:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_ADD_MILESTONE_TO_COMMUNITY_MERGE_REQUESTS'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/add-milestone-to-community-merge-requests.yml

ask-severity-priority-for-infradev-issues:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_ASK_SEVERITY_PRIORITY_FOR_INFRADEV_ISSUES'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/ask-severity-priority-for-infradev-issues.yml

ask-severity-for-sus-impacting-issues:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_ASK_SEVERITY_FOR_SUS_IMPACTING_ISSUES'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/ask-severity-for-sus-impacting-issues.yml
  needs:
    - job: label-sus-impacting-issues
      optional: true

close-old-issues:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_CLOSE_OLD_ISSUES'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/close-old-issues.yml

close-stale-bugs:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_CLOSE_STALE_BUGS'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/close-stale-bugs.yml

close-test-failure-issues:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_CLOSE_TEST_FAILURE_ISSUES'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/close-stale-test-failures.yml

discover:
  extends: .run
  stage: hygiene
  # Allow this job to fail for now, until https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/596 is fixed, otherwise it blocks the pipelines.
  allow_failure: true
  rules:
    - if: '$TRIAGE_DISCOVER'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/discover.yml

label-availability-issues:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_LABEL_AVAILABILITY_ISSUES'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/label-availability-issues.yml

label-sus-impacting-issues:
  extends: .run
  stage: pre-hygiene
  rules:
    - if: '$TRIAGE_LABEL_SUS_IMPACTING_ISSUES'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/label-sus-issues.yml

label-idle-community-mrs:
  extends: .run
  stage: pre-hygiene
  rules:
    - if: '$TRIAGE_LABEL_IDLE_COMMUNITY_MRS'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/label-idle-community-mrs.yml

label-community-contributions:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_LABEL_COMMUNITY_CONTRIBUTIONS'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/label-community-contributions.yml

label-bot-authored-mrs:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_LABEL_BOT_MRS'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/label-bot-authored-merge-requests.yml

label-jihu-contributions:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_LABEL_JIHU_CONTRIBUTIONS'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/label-jihu-contribution.yml

label-contractor-contributions:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_LABEL_CONTRACTOR_CONTRIBUTIONS'
  variables:
    TRIAGE_POLICY_FILE: policies/groups/gitlab-org/hygiene/label-contractor-contribution.yml

label-estimation-completed:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_LABEL_ESTIMATION_COMPLETED'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/label-estimation-completed.yml

label-missed-slo:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_LABEL_MISSED_SLO'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/label-missed-slo.yml

label-reminders:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_LABEL_REMINDERS'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/label-reminders.yml

label-seeking-community-contributions:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_LABEL_SEEKING_COMMUNITY_CONTRIBUTIONS'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/label-seeking-community-contributions.yml

label-vulnerability:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_LABEL_VULNERABILITY'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/label-vulnerability.yml

add-legal-disclaimer-to-direction-epics:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_ADD_LEGAL_DISCLAIMER_TO_DIRECTION_EPICS'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/add-legal-disclaimer-to-direction-epics.yml

gitlab-org:stage-and-group-labels-hygiene:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_STAGE_AND_GROUP_LABELS_HYGIENE'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/stage-and-group-labels-hygiene.yml
    TRIAGE_SOURCE_TYPE: groups
    TRIAGE_SOURCE_PATH: 9970 # gitlab-org

set-priority-from-severity:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_SET_PRIORITY_FROM_SEVERITY'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/set-priority-from-severity.yml

schedule-s1-bugs:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_S1_BUGS_HYGIENE'
  variables:
    TRIAGE_POLICY_FILE: policies/groups/gitlab-org/hygiene/schedule-s1-bugs.yml

remind-vulnerability-slo:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_REMIND_VULNERABILITY_SLO'
  variables:
    TRIAGE_POLICY_FILE: policies/groups/gitlab-org/hygiene/comment-vulnerability-issue-slo.yml

gitlab-org:composition-analysis:refinement:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_COMPOSITION_ANALYSIS_REFINEMENT_HYGIENE'
  variables:
    TRIAGE_POLICY_FILE: policies/groups/gitlab-org/composition-analysis/assign-refinement.yml
    TRIAGE_SOURCE_TYPE: groups
    TRIAGE_SOURCE_PATH: 9970 # gitlab-org

gitlab-org:threat-insights:weights:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_THREAT_INSIGHTS_WEIGHT_HYGIENE'
  variables:
    TRIAGE_POLICY_FILE: policies/groups/gitlab-org/threat-insights/issue-weights.yml
    TRIAGE_SOURCE_TYPE: groups
    TRIAGE_SOURCE_PATH: 9970 # gitlab-org

gitlab-org:threat-insights:refinement:
  extends: gitlab-org:threat-insights:weights
  rules:
    - if: '$TRIAGE_THREAT_INSIGHTS_REFINEMENT_HYGIENE'
  variables:
    TRIAGE_POLICY_FILE: policies/groups/gitlab-org/threat-insights/assign-refinement.yml

gitlab-org:threat-insights:tech-stack-label:
  extends: gitlab-org:threat-insights:weights
  rules:
    - if: '$TRIAGE_THREAT_INSIGHTS_TECH_STACK_LABEL'
  variables:
    TRIAGE_POLICY_FILE: policies/groups/gitlab-org/threat-insights/tech-stack-label.yml

gitlab-org:threat-insights:remove-assignee:
  extends: gitlab-org:threat-insights:weights
  rules:
    - if: '$TRIAGE_THREAT_INSIGHTS_REMOVE_ASSIGNEE'
  variables:
    TRIAGE_POLICY_FILE: policies/groups/gitlab-org/threat-insights/remove-assignee.yml

gitlab-org:threat-insights:internal-bug-triage:
  extends: gitlab-org:threat-insights:weights
  rules:
    - if: '$TRIAGE_THREAT_INSIGHTS_INTERNAL_BUG_TRIAGE'
  variables:
    TRIAGE_POLICY_FILE: policies/groups/gitlab-org/threat-insights/internal-bug-triage.yml

gitlab-org:security-policies:weights:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_SECURITY_POLICIES_WEIGHT_HYGIENE'
  variables:
    TRIAGE_POLICY_FILE: policies/groups/gitlab-org/security-policies/issue-weights.yml
    TRIAGE_SOURCE_TYPE: groups
    TRIAGE_SOURCE_PATH: 9970 # gitlab-org

gitlab-org:security-policies:refinement:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_SECURITY_POLICIES_REFINEMENT_HYGIENE'
  variables:
    TRIAGE_POLICY_FILE: policies/groups/gitlab-org/security-policies/assign-refinement.yml
    TRIAGE_SOURCE_TYPE: groups
    TRIAGE_SOURCE_PATH: 9970 # gitlab-org

gitlab-org:security-policies:remove-assignee:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_SECURITY_POLICIES_REMOVE_ASSIGNEE'
  variables:
    TRIAGE_POLICY_FILE: policies/groups/gitlab-org/security-policies/remove-assignee.yml
    TRIAGE_SOURCE_TYPE: groups
    TRIAGE_SOURCE_PATH: 9970 # gitlab-org

gitlab-org:dast:refinement:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_DAST_REFINEMENT_HYGIENE'
  variables:
    TRIAGE_POLICY_FILE: policies/groups/gitlab-org/dast/assign-refinement.yml
    TRIAGE_SOURCE_TYPE: groups
    TRIAGE_SOURCE_PATH: 9970 # gitlab-org

prompt-for-tier-labels:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_PROMPT_FOR_TIER_LABELS'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/prompt-for-tier-labels.yml

warn-impending-slo-breach:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_WARN_IMPENDING_SLO_BREACH'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/comment-slo-breaching-bugs.yml

gitlab-org:manage-authentication_and_authorization:feature-flag-check:
  extends: .run
  stage: hygiene
  rules:
    - if: '$MANAGE_ACCESS_FEATURE_FLAG_CHECK'
  variables:
    TRIAGE_POLICY_FILE: policies/groups/gitlab-org/manage-authentication_and_authorization/feature-flag-check.yml

gitlab-org:ide:remote-development-workflow:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_IDE_REMOTE_DEVELOPMENT_WORKFLOW'
  variables:
    TRIAGE_POLICY_FILE: policies/groups/gitlab-org/ide/remote-development-workflow.yml
    TRIAGE_SOURCE_TYPE: groups
    TRIAGE_SOURCE_PATH: 9970 # gitlab-org

prompt-for-documentation-label:
  extends: .run
  stage: hygiene
  rules:
    - if: '$PROMPT_FOR_DOCUMENTATION_LABEL'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/prompt-for-documentation-label.yml

prompt-for-vulnerability-label:
  extends: .run
  stage: hygiene
  rules:
    - if: '$PROMPT_FOR_VULNERABILITY_LABEL'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/prompt-for-vulnerability-label.yml

gitlab-org:global-search:weights:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_GLOBAL_SEARCH_WEIGHT_HYGIENE'
  variables:
    TRIAGE_POLICY_FILE: policies/groups/gitlab-org/global-search/issue-weights.yml
    TRIAGE_SOURCE_TYPE: projects
    TRIAGE_SOURCE_PATH: 278964 # gitlab-org/gitlab

nudge-stale-community-contributions:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_NUDGE_STALE_COMMUNITY_CONTRIBUTIONS'
  variables:
    TRIAGE_POLICY_FILE: policies/stages/hygiene/nudge-stale-community-contributions.yml

community:ping-inactive-author:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_COMMUNITY_PING_INACTIVE_AUTHOR'
  variables:
    TRIAGE_POLICY_FILE: policies/community/hygiene/ping-inactive-author.yml

community:ping-inactive-reviewers:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_COMMUNITY_PING_INACTIVE_REVIEWERS'
  variables:
    TRIAGE_POLICY_FILE: policies/community/hygiene/ping-inactive-reviewers.yml

community-ping-incorrect-review-label:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_COMMUNITY_PING_INCORRECT_REVIEW_LABEL'
  variables:
    TRIAGE_POLICY_FILE: policies/community/hygiene/ping-incorrect-review-label.yml

growth-team-refine-automation:
  extends: .run
  stage: hygiene
  rules:
    - if: '$TRIAGE_GROWTH_REFINE_AUTOMATION'
  variables:
    TRIAGE_POLICY_FILE: policies/groups/gitlab-org/growth/refine-automations.yml

label-gitaly-customer-bugs:
  extends: .run
  stage: hygiene
  rules:
    - if: '$LABEL_GITALY_CUSTOMER_BUGS'
  variables:
    TRIAGE_POLICY_FILE: policies/groups/gitlab-org/gitaly/gitaly-customer-issues-bugs.yml
    TRIAGE_SOURCE_TYPE: groups
    TRIAGE_SOURCE_PATH: 9970 # gitlab-org
